import React, { Component } from "react";
import { connect } from "react-redux";
import {THEM_SINH_VIEN, UPDATE_SINH_VIEN} from './redux/constant/svAction'


class FormInput extends Component {
  // tạo state mới lấy dữ liệu trong form truyền lên store
  state = {
    svNew: [
      {
        id: '',
        hoTen: '',
        phone: '',
        email: '',
      },
    ],
    errors: {
      id: '',
      hoTen: '',
      phone: '',
      email: '',
    },
    valid: false,
    svListCheck: [],
  };

  handleGetInput = (event) => {
    // bóc tách value từ event.target. Ngoài ra thêm thuộc tính name vào trong form's input bên dưới, để có thể gọi hàm dùng chung cho tất cả input.
    let { value, name, type, pattern } = event.target;
    let errorMsg = '';
    // CHECK ALL INPUT FORMAT
    // CASE: NULL
    if (value.trim() === ''){ // <-- nếu ô input rỗng, ko tính khoảng trắng đầu cuối (trim())
      errorMsg = name + ' không được để trống!';
    }
    // CASE: NOT NULL
    // check riêng cho ID vì có trường hợp không để trùng ID
    if (value.trim() !== '' && (type === 'email' || type === 'text')){
      if (name === "id"){
        const regex = new RegExp(pattern);
        if (!regex.test(value)){
          errorMsg = name + ' không đúng định dạng hoặc yêu cầu!'
        }
        let idValue = {[name]:value}
        let svListCheck = this.props.svList.map(item => item.id)
        console.log('svListCheck',svListCheck); //-> output là array với dãy số ID [1,2,3,4,5]

        // tạo vòng lặp so sánh nếu từng phần tử trong array trùng với giá trị ID nhập vào (dùng chung format Number để dấu so sánh === hoạt động) => hiện lỗi trùng
        svListCheck.forEach(element => {
          if (Number(element) === Number(idValue.id)){
            errorMsg = name + ' trùng với mã SV đã khởi tạo!'
          }
        })
      }
      // check cho các input còn lại (chỉ còn check format valid)
      else{
        const regex = new RegExp(pattern);
        if (!regex.test(value)){
          errorMsg = name + ' không đúng định dạng hoặc yêu cầu!'
        }
      }
    } 

    // update value/error
    let svNew = { ...this.state.svNew,[name]:value};
    let errors = {...this.state.errors,[name]:errorMsg}
    // reset state
    this.setState(
      {
        ...this.state,
        svNew: svNew,
        errors: errors,
      },
      () => {
        console.log(this.state);
        this.checkValidation();
      }
    );
  };

  // static getDerivedStateFromProps (newProps, currentState){
  //     // newProps: prop mới, prop cũ: this.prop <- ko truy xuất được do hàm static
  //     // currentState: ứng với state hiện tại (this.state)

  //     // hoặc trả về state mới
  //     let newState = {...currentState, svNew: newProps.svEdited }
  //     return newState;

  //     // trả về null, state giữ nguyên
  //     // return null;
  // }
//   ==> Do sau khi hàm getDerivedStateFromProps chạy thì render lại giao diện nên sẽ không thay đổi dc giá trị cần edit => dùng hàm componentDidUpdate, chạy sau render nhưng có khả năng truy xuất previous props/state

  // set condition of validation to enable Add Button
  checkValidation =()=>{
    let valid = true;
    // kiểm tra lỗi
    for (let item in this.state.errors){
      // nếu như 1 trong các input có lỗi (errorMsg !== '')
      if (this.state.errors[item] !== ''){
        valid = false;
      }
    }
    // kiểm tra rỗng
    if (valid === true){
      //  nếu valid = true, nhưng giá trị nhập vào của input là rỗng thì vần set valid = false
      for (let item in this.state.svNew){
        if (this.state.svNew[item] === ''){
          valid = false;
        }
      }
    }
    // kiểm tra trùng
    
    // cập nhật state cho valid
    this.setState({valid:valid})
  }

  render() {
    return (
      <div>
        <div className="row pb-5">
          <div className="col-12">
            <h2 className="bg-dark text-light py-3 pl-2 text-left">
              Thông tin sinh viên
            </h2>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Mã SV</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^\d+$"
              className="form-control"
              name="id" placeholder="Vui lòng nhập số"
              value={this.state.svNew.id || ''}
              required
            />
            <span className="text-danger">{this.state.errors.id}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Họ tên</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*)[a-zA-Z\s]{5,}$"
              className="form-control"
              name="hoTen" placeholder="Vui lòng nhập tên không dấu với ít nhất 1 chữ cái"
              value={this.state.svNew.hoTen || ''}
              required
            />
            <span className="text-danger">{this.state.errors.hoTen}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Số điện thoại</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^(0|\+84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$"
              className="form-control"
              name="phone" placeholder="Vui lòng nhập số theo tiêu chuẩn Việt Nam"
              value={this.state.svNew.phone || ''}
              required
            />
            <span className="text-danger">{this.state.errors.phone}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Email</p>
            <input
              onChange={this.handleGetInput}
              type="email" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
              className="form-control"
              name="email" placeholder="Vui lòng nhập đúng định dạng x@y.z"
              value={this.state.svNew.email || ''}
              required
            />
            <span className="text-danger">{this.state.errors.email}</span>
          </div>
          <div className="col-12 text-right mt-1">
            {/* set điều kiện ẩn hiện nút thêm */}
            {this.state.valid ? <button onClick={() => {this.props.handleAddSV(this.state.svNew);}} className="btn btn-success py-2 text-left mr-2">Thêm sinh viên</button> : <button onClick={() => {this.props.handleAddSV(this.state.svNew);}} className="btn btn-success py-2 text-left mr-2" disabled>Thêm sinh viên</button>}
            {/* set điều kiện ẩn hiện nút update */}
            {this.state.valid ? <button onClick={() => {this.props.handleUpdateSV(this.state.svNew);}} className="btn btn-warning py-2 text-left">Cập nhật sinh viên</button> : <button onClick={() => {this.props.handleUpdateSV(this.state.svNew);}} className="btn btn-warning py-2 text-left" disabled>Cập nhật sinh viên</button>}
          </div>
        </div>
      </div>
    );
  }
  // chạy sau render, nhưng đây là lifecycle trả về props cũ và state cũ của component trước khi render
  componentDidUpdate(prevProps, prevState){
    // trước tiên, để tránh tình trạng vòng lặp vô tận (setState rồi render lại, rồi lại setState...), chúng ta cần lệnh so sánh nếu props trước đó (svEdit trước mà khác với svEdit hiện tại thì mới setState)
    if (prevProps.svEdited.id !== this.props.svEdited.id){
        this.setState({
            svNew: this.props.svEdited
        })
    } 

  }
}

let mapStateToProps = (state) => {
  return {
    svList: state.formReducer.svList,
    svEdited: state.formReducer.svEdited,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddSV: (sinhVien) => {
      const action = {
        type: THEM_SINH_VIEN,
        sinhVien,
      };
      dispatch(action);
    },
    handleUpdateSV: (sinhVien) => {
        const action = {
          type: UPDATE_SINH_VIEN,
          sinhVien,
        };
        dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormInput);
