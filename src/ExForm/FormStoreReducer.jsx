import React, { Component } from 'react'
import FormInput from './FormInput'
import FormTable from './FormTable'

export default class FormStoreReducer extends Component {
  render() {
    return (
      <div className='container py-5'>
        {/* form input */}
        <FormInput></FormInput>
        {/* form table */}
        <FormTable></FormTable>
      </div>
    )
  }
}
