import {THEM_SINH_VIEN, UPDATE_SINH_VIEN, XOA_SINH_VIEN, EDIT_SINH_VIEN, SEARCH_SINH_VIEN} from '../constant/svAction'

const initialState = {
  svList: [
    {
      id: 1,
      hoTen: 'Tran Tuan Anh',
      phone: '0909572536',
      email: 'tuananh561984@gmail.com',
    },
    {
      id: 2,
      hoTen: 'Tran Anh Khoi',
      phone: '0909090909',
      email: 'anhkhoi212@gmail.com',
    },
    {
      id: 3,
      hoTen: 'Pham Quynh Nhu',
      phone: '0987654321',
      email: 'quynhnhu@gmail.com',
    },
    {
      id: 4,
      hoTen: 'Nguyen Quang Linh',
      phone: '0938776688',
      email: 'quanglinh@gmail.com',
    },
    {
      id: 5,
      hoTen: 'Tran Minh Tri',
      phone: '0989990088',
      email: 'minhtri@gmail.com',
    },
  ],
  svEdited: {
    id: null,
    hoTen: '',
    phone: '',
    email: '',
  },
  keyword: '',
  isSearch: false,
  svListSearched: [],
}

export const formReducer = (state = initialState, action) => {
  switch (action.type){
    case THEM_SINH_VIEN: {
      state.isSearch = false;
      let svListUpdated = [...state.svList, action.sinhVien];
      console.log('1. sinh vien added: ',action.sinhVien);
      return {...state, svList:svListUpdated}
    }
    case XOA_SINH_VIEN: {
      state.isSearch = false;
      let clonedSVList = [...state.svList];
      clonedSVList.splice(action.index, 1);
      console.log('2. index removed: ',action.index)
      return {...state, svList:clonedSVList}
    }
    case EDIT_SINH_VIEN: {
      state.isSearch = false;
      return {...state, svEdited: action.sinhVien}
    }
    case UPDATE_SINH_VIEN: {
      state.isSearch = false;
      state.svEdited = {...state.svEdited, action}
      // console.log('1. action: ',action);
      // console.log('1. svEdited: ', state.svEdited);
      let svListUpdate = [...state.svList];
      // console.log('2. svListUpdate: ', svListUpdate)
      let index = svListUpdate.findIndex(item => item.id === state.svEdited.id)
      console.log('3. sinh viên edited: ', action.sinhVien)
      // console.log('3. index: ', index);
      if (index !== -1){
        svListUpdate[index] = action.sinhVien;
        // console.log('4. state.svEdited: ', state.svEdited);
        // console.log('5. svListUpdate: ', svListUpdate);
      }
      return {...state, svList:svListUpdate}
    }
    case SEARCH_SINH_VIEN: {
      let svListFiltered = [...state.svList]
      if (action.keyword === ''){
        return {...state, svListSearched: state.svList}
      } else 
      // set giá trị true cho isSearch để kích hoạt mảng render trong table là mảng từ array svListSearched. Trong các case còn lại set lại giá trị là false để mảng render là svList bình thường
      state.isSearch = true;
      if (action.keyword.length > 1){
        let finalList = svListFiltered.filter(keyword => keyword.hoTen.toLowerCase().indexOf(action.keyword) > -1)
        console.log('4. action.keyword: ',action.keyword );
        console.log('5. finalList: ', finalList);
        console.log('isSearched (true/false): ',state.isSearch)
        return {...state, svListSearched: finalList}
      }
    }
    // eslint-disable-next-line no-fallthrough
    default: {
      return state
    }
  }
} 