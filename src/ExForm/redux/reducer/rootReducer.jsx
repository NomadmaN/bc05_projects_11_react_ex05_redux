import {combineReducers} from 'redux'
import {formReducer} from './formReducer'

export const rootReducer_Form = combineReducers({
    formReducer:formReducer,
})